#!/bin/bash

function rcon {
	/usr/local/bin/mcrcon -H 127.0.0.1 -P 25575 -p ${RCON_PASSWORD} "$1"
}

today=$(date +%F-%H-%M)

source="$HOME/server"
target="$HOME/backups"

rcon "save-off"
rcon "save-all"
tar -cvpzf "$target/backup_$today.tar.gz" "$source"
rcon "save-on"

## Delete older backups
find "$target" -type f -mtime +7 -name ‘*.gz’ -delete
